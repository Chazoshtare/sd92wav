#include <iostream>
#include <fstream>
#include "sd9.h"

bool verifyHeader(const sd9Header_t header) {
    return header.sd == 0x394453
           && header.headerSize == 0x20
           && header.unknown2 == 0x40;
}

sd9Header_t readHeader(std::ifstream& file) {
    sd9Header_t header;
    file.read ((char*) &header, sizeof header);

    return header;
}
