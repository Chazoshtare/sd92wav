#include <iostream>
#include <fstream>
#include <filesystem>
#include "sd9.h"

void saveToFile(std::ifstream& source, std::string filename);
std::string toWavFilename(std::string sd9Filename);

static void showUsage(std::string name) {
    std::cerr << "Usage: " << name << " [option(s)] file1.sd9 file2.sd9 ...\n"
              << "Options:\n"
              << "\t-h, --help\t\t\tShow this help message\n"
              << "\t-d, --directory [DIRECTORY]\tOutput all results to a directory"
              << std::endl;
}

int main(int argc, char** argv) {

    if (argc <= 1) {
        showUsage(argv[0]);
        return 1;
    }

    std::string dirName = "";
    int argIndex = 1;
    std::string arg = argv[1];
    if (arg == "-h" || arg == "--help") {
        showUsage(argv[0]);
        return 0;

    } else if (arg == "-d" || arg == "--directory") {
        if (argc < 3) {
            std::cerr << "--directory option requires one argument." << std::endl;
            return 1;
        }

        dirName = argv[++argIndex];
        std::filesystem::path directory(dirName);
        if (!std::filesystem::exists(directory)) {
            std::filesystem::create_directory(directory);
        }
        dirName += "/";
        ++argIndex;
    }

    while (argIndex < argc) {
        std::ifstream source(argv[argIndex], std::ios::binary);

        if (source.good()) {
            sd9Header_t header = readHeader(source); // read 32 header bytes

            if (verifyHeader(header)) {
                std::string filename = "";
                if (dirName.empty()) {
                    filename = toWavFilename(std::string(argv[argIndex]));
                } else {
                    std::filesystem::path originalPath(argv[argIndex]);
                    filename = dirName + toWavFilename(originalPath.filename().string());
                }

                saveToFile(source, filename); // save the rest to destination file
                std::cout << "Sound successfully extracted to " << filename << std::endl;

            } else {
                std::cout << argv[argIndex] << " is not a valid SD9 file!" << std::endl;
            }
        } else {
            std::cout << "Can't open file " << argv[argIndex] << std::endl;
        }

        source.close();
        source.clear();

        ++argIndex;
    }

    return 0;
}

void saveToFile(std::ifstream& source, std::string filename) {
    std::ofstream destination(filename, std::ios::binary);
    destination << source.rdbuf(); // save the rest to destination file
    destination.close();
    destination.clear();
}

std::string toWavFilename(std::string sd9Filename) {
    if (sd9Filename.length() > 4) {
        sd9Filename.erase(sd9Filename.end() - 4, sd9Filename.end());
    }
    sd9Filename.append(".wav");

    return sd9Filename;
}
